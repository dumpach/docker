# Dumpach
## SPA imageboard

### Quick start

Install Docker and Docker Compose on your machine.

#### Local development

Run from project root
```
docker-compose -f development.docker-compose.yml up --build
```